public class AirConditioner {
    // All four fields
    public String brand;
    public double width;
    public double depth;
    public double height;

    /* Cost of Air conditioner :
     * Carrier : 200$
     * Goodman : 180$
     * Trane: 160$
     * Lennox: 140$
     * Rhem: 120$
     * Others: 100$
     * Each cm3 = 0.004$
    */
    
    //This method calculate the dimension of the appliance
    public static double dimension (double width, double height, double depth) {
        return (width*height*depth);
    }

    // This method calculate the cost of the air conditioner with his dimension
    public double applianceCost () {
        double size = dimension(width,height,depth);
        double startingPrice;
        double cost;
                switch (brand.toLowerCase()) {
                    case "carrier" :
                        startingPrice = 200;
                        break;
                    case "goodman" :
                        startingPrice = 180;
                        break;
                    case "trane" :
                        startingPrice = 160;
                        break;
                    case "lennox" :
                        startingPrice = 140;
                        break;
                    case "rhem" : 
                        startingPrice = 120;
                        break;
                    default :
                        startingPrice = 100;
                }
        cost = startingPrice + (0.004*size);
        return cost;
    }

    //This method gives all informations about the product in a list
    public void informations () {
        // Display string, width, depth, height, total dimension and cost
        double totalDimension = dimension(width,height,depth);
        double cost = applianceCost();

        System.out.printf("Here is all the information about the air conditioner : \nBrand : %s \nWidth : %.2f cm\nDepth : %.2f cm\nHeight : %.2f cm\nTotal dimension : %.2f cm3\nCost : %.2f$\n", brand, width, depth, height, totalDimension, cost);
    }
}
