import java.util.Scanner;
public class ApplianceStore {
    public static void main(String[]args) {
        // Create new array of 4 appliances
        AirConditioner[] airConditioners = new AirConditioner[4];
        Scanner scan = new Scanner(System.in);

        for(int i = 0; i < airConditioners.length; i++){
            System.out.println("Input infomation for the Air conditioner number " + (i+1));
            airConditioners[i] = new AirConditioner();
            // Ask user input
            System.out.println("What brand is the Air conditioner?");
            airConditioners[i].brand = scan.next();

            System.out.println("What's the width of the Air conditioner?(cm)");
            airConditioners[i].width = scan.nextDouble();

            System.out.println("What's the depth of the Air conditioner?(cm)");
            airConditioners[i].depth = scan.nextDouble();

            System.out.println("What's the height of the Air conditioner?(cm)");
            airConditioners[i].height = scan.nextDouble();
        }   

        // Printing last appliance informations
        airConditioners[3].informations();

        //call both instance methods for first appliance
        System.out.println("This is the cost of the first air conditioner : " + airConditioners[0].applianceCost());
        airConditioners[0].informations();
    }
}
